/**
 * .section is a diractive to our assembler telling it to place this code first.
 * .globl is a directive to our assembler, that tells it to export this symbol
 * to the elf file. Convention dictates that the symbol _start is used for the 
 * entry point, so this all has the net effect of setting the entry point here.
 * Ultimately, this is useless as the elf itself is not used in the final 
 * result, and so the entry point really doesn't matter, but it aids clarity,
 * allows simulators to run the elf, and also stops us getting a linker warning
 * about having no entry point. 
 * 
 * TODO: how does this guarantee that the first instruction begins here?
 * as i understand it, the text section just adds code, but there is no specific entry point
 * in the linker script (_start is supposed to be a dummy? (see above) or isn't it??)
 * I need a way to make sure this is the VERY first code that gets executed
 * (I think this is usually done by making sure the main.o is linked FIRST)
 * It would be better to have no specific order in the linking step by letting
 * the linker script explicitly use the start symbol. However i *think* this is only
 * possible with ELF kernels (since they contain that LOAD POINT info in the binary image.
 * and the RASPI uses binary kernel images (that's i think what the firmware expects???)
 * UPDATE: the Raspbian kernel.img is NOT a binary image but a "ARM bootable zImage"
 * which seems to indicate other object formats are possible also.
 * I need to find out more about this.
 */
.section .init
.globl _start
_start:

/* Initialize the stack */
mov sp, #0x00008000

/* Invoke the main function */
bl redwoodMain
bl haltSystem
