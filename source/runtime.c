/**
 * I have no idea what this is.
 * It's just a symbol the D compiler generates, and the linker therefor needs.
 * It can be anything, i just chose it to be an int (which might be wrong, end thus give me
 * issues later.
 * TODO: Really need to find out what this is, actually.
 */
void* _Dmodule_ref = (void *)150783;
void* __aeabi_unwind_cpp_pr1 = (void *)150783;
void* _Unwind_Resume = (void *)150783;

/**
 * This is confirmed to be a function. (asm contains bl to it)
 * Parameters and return value are unclear.
 */
void _d_array_bounds(/* ??? */) {
}

/**
 * This is confirmed to be a function. (asm contains bl to it)
 * Parameters and return value are unclear.
 */
void __emutls_get_address(/* ??? */) {
}
