import led;
import timer;

const uint SCREEN_WIDTH = 640;
const uint SCREEN_HEIGHT = 480;
const uint SCREEN_BITS_PER_PIXEL = 24;

void changeDisplay() {	
	ubyte *frameBufferAddress = initializeFrameBuffer(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BITS_PER_PIXEL);
//	drawRectangle(frameBufferAddress);
//	drawNumber(frameBufferAddress, 150783);
	haltSystemWhileSignallingBoolean(frameBufferAddress != null);
}

void drawRectangle(ubyte *frameBufferAddress) {
	for(uint x = 0; x < SCREEN_WIDTH / 2; x++) {
		for(uint y = 0; y < SCREEN_HEIGHT / 2; y++) {
			drawPixel(frameBufferAddress, x, y, 0, 0, 127);
		}
	}
}

void drawPixel(ubyte *frameBufferAddress, uint x, uint y, ubyte red, ubyte green, ubyte blue) {
	uint bytesPerPixel = (SCREEN_BITS_PER_PIXEL / 8);
	frameBufferAddress[(x + (y * SCREEN_WIDTH)) * bytesPerPixel + 0] = red;
	frameBufferAddress[(x + (y * SCREEN_WIDTH)) * bytesPerPixel + 1] = green;
	frameBufferAddress[(x + (y * SCREEN_WIDTH)) * bytesPerPixel + 2] = blue;
}

ubyte *initializeFrameBuffer(uint width, uint height, uint bitsPerPixel) {
	// Just a random address that is (hopefully) not used for anything else.
	// (checked symbol table to find an address well outside the used range)
	// Must be aligned to 4 bits (4 LSB are 0)
	uint *frameBufferInfo = cast(uint *)0x00050000;
/*	ubyte[50] data;
	ubyte *dataPtr = cast(ubyte *)data;
	while((cast(uint)dataPtr & 0b1111) != 0) {
		dataPtr += 1;
	}
	uint *frameBufferInfo = cast(uint *)dataPtr;
*/
	frameBufferInfo[0] = width;
	frameBufferInfo[1] = height;
	frameBufferInfo[2] = width;
	frameBufferInfo[3] = height;
	frameBufferInfo[4] = 0; // Set by GPU
	frameBufferInfo[5] = bitsPerPixel;
	frameBufferInfo[6] = 0;
	frameBufferInfo[7] = 0;
	frameBufferInfo[8] = 0; // Set by GPU
	frameBufferInfo[9] = 0; // Set by GPU

	uint mailboxNumber = 1;
	sendMessageToMailbox(cast(uint)(cast(uint *)frameBufferInfo), mailboxNumber);
	uint message = readMessageFromMailbox(mailboxNumber);
	ubyte *frameBufferAddress = null;
	do {
		frameBufferAddress = cast(ubyte *)frameBufferInfo[8];
	} while(frameBufferAddress == null);

	drawRectangle(frameBufferAddress);

	return frameBufferAddress;

	// TODO:
	// It seems to be the case that after switching the display mode, the stack pointer, or lr or any of the status register
	// are majorly fucked up????????????? I have not clue on why, i'm pulling my hair out on this.
	// WHAT THE HELL IS GOING ON????
}

ubyte[640*480*3] fakeBuffer;

void drawNumber(ubyte *frameBufferAddress, uint number) {
	// TODO: validation of input

	uint position = 200;	
	if(number == 0) {
		drawLetter(frameBufferAddress, 0, position);
	} else {
		while(number != 0) {
			uint lastDigitValue = number % 10;
			drawLetter(frameBufferAddress, lastDigitValue, position);
			number /= 10;
			position -= (7 + 1);
		}
	}
}

void drawLetter(ubyte *frameBufferAddress, uint letter, uint position) {
	ubyte[10] letterData;
	switch(letter) {
		case 0:
			letterData[0] = 0b0011100;
			letterData[1] = 0b0110110;
			letterData[2] = 0b1100011;
			letterData[3] = 0b1100011;
			letterData[4] = 0b1101011;
			letterData[5] = 0b1101011;
			letterData[6] = 0b1100011;
			letterData[7] = 0b1100011;
			letterData[8] = 0b0110110;
			letterData[9] = 0b0011100;
			break;
		case 1:
			letterData[0] = 0b0001100;
			letterData[1] = 0b0011100;
			letterData[2] = 0b0111100;
			letterData[3] = 0b0001100;
			letterData[4] = 0b0001100;
			letterData[5] = 0b0001100;
			letterData[6] = 0b0001100;
			letterData[7] = 0b0001100;
			letterData[8] = 0b0001100;
			letterData[9] = 0b0111111;
			break;
		case 2:
			letterData[0] = 0b0111110;
			letterData[1] = 0b1100011;
			letterData[2] = 0b0000011;
			letterData[3] = 0b0000110;
			letterData[4] = 0b0001100;
			letterData[5] = 0b0011000;
			letterData[6] = 0b0110000;
			letterData[7] = 0b1100000;
			letterData[8] = 0b1100011;
			letterData[9] = 0b1111111;
			break;
		case 3:
			letterData[0] = 0b0111110;
			letterData[1] = 0b1100011;
			letterData[2] = 0b0000011;
			letterData[3] = 0b0000011;
			letterData[4] = 0b0011110;
			letterData[5] = 0b0000011;
			letterData[6] = 0b0000011;
			letterData[7] = 0b0000011;
			letterData[8] = 0b1100011;
			letterData[9] = 0b0111110;
			break;
		case 4:
			letterData[0] = 0b0000110;
			letterData[1] = 0b0001110;
			letterData[2] = 0b0011110;
			letterData[3] = 0b0110110;
			letterData[4] = 0b1100110;
			letterData[5] = 0b1111111;
			letterData[6] = 0b0000110;
			letterData[7] = 0b0000110;
			letterData[8] = 0b0000110;
			letterData[9] = 0b0001111;
			break;
		case 5:
			letterData[0] = 0b1111111;
			letterData[1] = 0b1100000;
			letterData[2] = 0b1100000;
			letterData[3] = 0b1100000;
			letterData[4] = 0b1111110;
			letterData[5] = 0b0000011;
			letterData[6] = 0b0000011;
			letterData[7] = 0b0000011;
			letterData[8] = 0b1100011;
			letterData[9] = 0b0111110;
			break;
		case 6:
			letterData[0] = 0b0011100;
			letterData[1] = 0b0110000;
			letterData[2] = 0b1100000;
			letterData[3] = 0b1100000;
			letterData[4] = 0b1111110;
			letterData[5] = 0b1100011;
			letterData[6] = 0b1100011;
			letterData[7] = 0b1100011;
			letterData[8] = 0b1100011;
			letterData[9] = 0b0111110;
			break;
		case 7:
			letterData[0] = 0b1111111;
			letterData[1] = 0b1100011;
			letterData[2] = 0b1000011;
			letterData[3] = 0b0000011;
			letterData[4] = 0b0000110;
			letterData[5] = 0b0000110;
			letterData[6] = 0b0001100;
			letterData[7] = 0b0001100;
			letterData[8] = 0b0011000;
			letterData[9] = 0b0011000;
			break;
		case 8:
			letterData[0] = 0b0111110;
			letterData[1] = 0b1100011;
			letterData[2] = 0b1100011;
			letterData[3] = 0b1100011;
			letterData[4] = 0b0111110;
			letterData[5] = 0b1100011;
			letterData[6] = 0b1100011;
			letterData[7] = 0b1100011;
			letterData[8] = 0b1100011;
			letterData[9] = 0b0111110;
			break;
		case 9:
			letterData[0] = 0b0111110;
			letterData[1] = 0b1100011;
			letterData[2] = 0b1100011;
			letterData[3] = 0b1100011;
			letterData[4] = 0b0111111;
			letterData[5] = 0b0000011;
			letterData[6] = 0b0000011;
			letterData[7] = 0b0000011;
			letterData[8] = 0b0000110;
			letterData[9] = 0b0111100;
			break;
		default:
			letterData[0] = 0b0101010;
			letterData[1] = 0b1010101;
			letterData[2] = 0b0101010;
			letterData[3] = 0b1010101;
			letterData[4] = 0b0101010;
			letterData[5] = 0b1010101;
			letterData[6] = 0b0101010;
			letterData[7] = 0b1010101;
			letterData[8] = 0b0101010;
			letterData[9] = 0b1010101;
			break;
	}

	uint letterWidth = 7;
	uint letterHeight = 10;

	for(uint y = 0; y < letterHeight; y++) {
		for(uint x = 0; x < letterWidth; x++) {
			ubyte value = ((letterData[y] >> letterWidth - 1 - x) & 1) == 1 ? 255 : 0;
			drawPixel(frameBufferAddress, position + x, y, value, value, value);
		}
	}
}

void sendMessageToMailbox(uint message, uint mailbox) {
	// TODO: validate input!

	// Wait until the status field (0x2000B898) has 0 in the 31st bit (0x80000000).
	uint* statusFieldAddress = cast(uint*)0x2000B898;
	while((*statusFieldAddress & 0x80000000) != 0) {
	}
	
	uint value = message | mailbox;

	uint *writeAddress = cast(uint *)0x2000B8A0;
	*writeAddress = value;
}

uint readMessageFromMailbox(uint mailbox) {
	// TODO: validate input!

	// Wait until the status field (0x2000B898) has 0 in the 30th bit (0x40000000).
	uint* statusFieldAddress = cast(uint*)0x2000B898;
	while((*statusFieldAddress & 0x40000000) != 0) {
	}

	uint *readAddress = cast(uint *)0x2000B880;
	uint value = 0;
	do {
		value = *readAddress;
	} while((value & 0b1111) != mailbox);

	return value & !0b1111;
}

