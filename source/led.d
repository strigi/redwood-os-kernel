import timer;

/**
 * Sets the GPIO 16 function select.
 * This 'prepares' the GPIO controller for setting / unsetting the LED.
 * A function select should be repeated once before doing anything with a distinct GPIO pin.
 * When dealing with only one GPIO pin, repeating the function select is thus not required.
 */
void setLedFunctionSelect() {
	uint* address = cast(uint*)0x20200004;
	*address = 0x00040000;
}

/**
 * Set GPIO 16 to low, causing the LED to turn on.
 */
void turnLedOn() {
	uint* address = cast(uint*)0x20200028;
	*address = 0x00010000;
}

/**
 * Set CPIO 16 to high, causing the LED to turn off.
 */
void turnLedOff() {
	uint* address = cast(uint*)0x2020001C;
	*address = 0x00010000;
}

void countDownFrom(uint start) {
	for(uint i = start; i > 0; i--) {
		flashCounter(i, 200);
		wait(1000 * 1000);
	}
}

void flashCounter(uint count, uint delayMilliseconds) {
	for(uint i = 0; i < count; i++) {
		turnLedOn();
		wait(delayMilliseconds * 1000);
		turnLedOff();
		wait(delayMilliseconds * 1000);
	}
}

/**
 * Puts the system in an infinite loop that signals true or false
 * by means of the status led.
 * True is signalled by two short flashes
 * False is signalled by one long flash
 */ 
void haltSystemWhileSignallingBoolean(bool yes) {
	if(yes) {
		while(true) {
			flashCounter(2, 100);
			wait(1700 * 1000);
		}
	} else {
		while(true) {
			flashCounter(1, 1000);
		}
	}
}
