/**
 * Retrieves the internal system timer value.
 * This is a value that accurately increments every 1 µs.
 * @return The current system timer value as an ulong.
 */
ulong getSystemTimer() {
	ulong* address = cast(ulong*)0x20003004;
	return *address;
}

void wait(uint delayInMicroSeconds) {
	ulong startValue = getSystemTimer();
	ulong targetValue = startValue + delayInMicroSeconds;
	while(getSystemTimer() < targetValue) {
	}
}
