import led;
import timer;

/**
 * Constants
 */
const uint UNIT_LENGTH = 300000;

const uint D = 0b100;
const uint D_SIZE = 3;

const uint E = 0b0;
const uint E_SIZE = 1;

const uint O = 0b111;
const uint O_SIZE = 3;


const uint R = 0b010;
const uint R_SIZE = 3;

const uint W = 0b011;
const uint W_SIZE = 3;

void sendMessage() {
	while(true) {
		sendCharacter(R, R_SIZE);
		sendCharacter(E, E_SIZE);
		sendCharacter(D, D_SIZE);
		sendCharacter(W, W_SIZE);
		sendCharacter(O, O_SIZE);
		sendCharacter(O, O_SIZE);
		sendCharacter(D, D_SIZE);
		wait(4);
	}
}

void sendCharacter(uint character, uint numberOfSignals) {
	uint unitLengthInMicroSeconds = 100 * 1000;
	uint mask = 0b1 << (numberOfSignals - 1);
	while(mask != 0) {
		turnLedOn();
		if(isDot(character, mask)) {
			wait(unitLengthInMicroSeconds);
		} else {
			wait(3 * unitLengthInMicroSeconds);
		}
		turnLedOff();
		wait(1 * unitLengthInMicroSeconds);
		mask >>= 1;
	}
	wait(2 * unitLengthInMicroSeconds);
}

bool isDot(uint character, uint mask) {
	return character & (mask != 0);
}
