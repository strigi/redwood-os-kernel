import led;
import timer;
import display;

/**
 * The main kernel entry point.
 */
extern(C) void redwoodMain() {
	// TODO: should this be repeated before each access to the status led?
	setLedFunctionSelect();

	changeDisplay();
}

extern(C) void haltSystem() {
	while(true) {
		turnLedOn();
		wait(100 * 1000);
		turnLedOff();
		wait(100 * 1000);
	}
}
