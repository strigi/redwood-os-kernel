#define WHITE_TXT = 0x07

void k_clear_screen();
unsigned int k_printf(char *message, unsigned int line);
void update_cursor(int row, int column);

k_main() {
	k_clear_screen();
	k_printf("Hello World!", 0);
}

void k_clear_screen() // clear the entire text screen
{
	char *vidmem = (char *) 0xb8000;
	unsigned int i=0;
	while(i < (80*25*2))
	{
		vidmem[i]=' ';
		i++;
		vidmem[i] = 0x07;
		i++;
	};
};

unsigned int k_printf(char *message, unsigned int line) // the message and then the line #
{
	char *vidmem = (char *) 0xb8000;
	unsigned int i=0;

	i=(line*80*2);

	while(*message!=0)
	{
		if(*message=='\n') // check for a new line
		{
			line++;
			i=(line*80*2);
			*message++;
		} else {
			vidmem[i]=*message;
			*message++;
			i++;
			vidmem[i] = 0x07;
			i++;
		};
	};

	return(1);
};
