static const ubyte *VIDEO_MEMORY_BASE_ADDRESS = cast(ubyte *)0xB8000;
static const int NUMBER_OF_CHARACTERS_PER_LINE = 80;

static const int PAGE_SIZE = 4096;

static int videoMemoryIndex = 0;

private void printCharacter(char character) {
	if(character == '\n') {
		videoMemoryIndex = (videoMemoryIndex / NUMBER_OF_CHARACTERS_PER_LINE) * NUMBER_OF_CHARACTERS_PER_LINE + NUMBER_OF_CHARACTERS_PER_LINE;
	} else {
		int cursor = videoMemoryIndex * 2;
		VIDEO_MEMORY_BASE_ADDRESS[cursor] = character;
		VIDEO_MEMORY_BASE_ADDRESS[cursor + 1] = 0x1F;
		videoMemoryIndex++;
	}
}

private void printInteger(uint integer) {
	const int RADIX = 16;
	const char[] characters = "0123456789ABCDEF";
	char[64] buffer;
	
	int i;
	for(i=0; integer > 0; i++) {
		int characterIndex = integer % RADIX;
		integer /= RADIX;
		buffer[i] = characters[characterIndex];
	}
	i--;
	
	if(i == -1) {
		printCharacter('0');
	} else {
		while(i >= 0) {
			printCharacter(buffer[i]);
			i--;
		}
	}
}

private void printLong(ulong integer) {
	const int RADIX = 16;
	const char[] characters = "0123456789ABCDEF";
	char[64] buffer;
	
	int i;
	for(i=0; integer > 0; i++) {
		int characterIndex = integer % RADIX;
		integer /= RADIX;
		buffer[i] = characters[characterIndex];
	}
	i--;
	
	if(i == -1) {
		printCharacter('0');
	} else {
		while(i >= 0) {
			printCharacter(buffer[i]);
			i--;
		}
	}
}

private void printPointers(byte *begin, byte *end) {
	printInteger(cast(uint)begin);
	printCharacter(' ');
	printCharacter('-');
	printCharacter(' ');
	printInteger(cast(uint)end);
	printCharacter('\n');
}

private void kernelMain(byte *codePageBeginAddress, byte *codePageEndAddress
		, byte *dataPageBeginAddress, byte *dataPageEndAddress
		, byte *stackPageBeginAddress, byte *stackPageEndAddress) {
 	printCharacter('C');
	printCharacter(' ');
	printPointers(codePageBeginAddress, codePageEndAddress);
	
	printCharacter('D');
	printCharacter(' ');
	printPointers(dataPageBeginAddress, dataPageEndAddress);
	
	printCharacter('S');
	printCharacter(' ');
	printPointers(stackPageBeginAddress, stackPageEndAddress);

    // SETUP PAGING
	// TODO: Assuming the stack is mapped last!!!
	uint *pageDirectory = cast(uint *)(stackPageEndAddress + 1);
	uint *pageTable = cast(uint *)(stackPageEndAddress + 1 + PAGE_SIZE);

	printCharacter('\n');printCharacter('P');printCharacter('D');printCharacter(' ');
    printInteger(cast(uint)pageDirectory);
    printCharacter('\n');printCharacter('P');printCharacter('T');printCharacter(' ');
	printInteger(cast(uint)pageTable);
	
    // Map the first 4 MB of memory
    // It is currently assumed that the kernel, the page dir and page table
    // are themselves contained in the first 4MB.
    // No idea if this is good, bad, required, or whatever.
    // Also, the current mapping maps 1:1 real memory, to virtual memory
    uint address = 0;
    for(int i=0; i<1024; i++) {
        uint pageTableEntry = createPageTableEntry(true, true, true, false, false, 0, address);
        pageTable[i] = pageTableEntry;
        address += PAGE_SIZE;
    }
    pageDirectory[0] = createPageTableEntry(true, true, true, false, false, 0, cast(uint)pageTable);
    for(int i=1; i<1024; i++) {
        pageDirectory[i] = createPageTableEntry(false, true, true, false, false, 0, cast(byte *)0);
    }
}

/**
 * Checks if the boolean condition is in fact true.
 * params:
 *      condition = The condition that must be true.
 */
private void assertTrue(bool condition) {
    if(!condition) {
        printCharacter('A');
        printCharacter('S');
        printCharacter('S');
        printCharacter('E');
        printCharacter('R');
        printCharacter('T');
        printCharacter('I');
        printCharacter('O');
        printCharacter('N');
        printCharacter(' ');
        printCharacter('F');
        printCharacter('A');
        printCharacter('I');
        printCharacter('L');
        printCharacter('E');
        printCharacter('D');
        
        while(true) {
        }
    }
}

/**
 * Creates a page table entry, describing a page mapping as specified by the parameters.
 * params:
 *      present     = true if the page is currently in memory.
 *      writable    = true if the page should be read/write, false if the page should be readonly.
 *      supervisor  = true if this is a supervisor page, false if this is a user page.
 *      accessed    = true if this page is accessed.
 *      dirty       = true if this page is dirty.
 *      available   = provides the programmer with 3 bits (only the lowest 3 may be used) for his/her own use.
 *      pageAddress = the full address of the page to be mapped. Since this must be a page address, it must be a multiple of 4096.
 * returns:
 *      a fully constructed page table entry.
 */
private uint createPageTableEntry(bool present, bool writable, bool supervisor, bool accessed, bool dirty, ubyte available, uint pageAddress) {
    // Assert the constract of this method
    assertTrue(available <= 7);  // Ony the first 3 bits may be used (111 binary = 7 decimal)
    assertTrue(pageAddress % PAGE_SIZE == 0);
    
    uint pageTableEntry = 0;

    pageTableEntry |= cast(uint)present << 0;           // bit 0        : present
    pageTableEntry |= cast(uint)writable << 1;          // bit 1        : readonly / readwrite
    pageTableEntry |= cast(uint)!supervisor << 2;       // bit 2        : supervisor / user
                                                        // bit 3..4     : reserved (set to 0)
    pageTableEntry |= cast(uint)accessed << 5;          // bit 5        : accessed
    pageTableEntry |= cast(uint)dirty << 6;             // bit 6        : dirty
                                                        // bit 7..8     : reserved (set to 0)
    pageTableEntry |= cast(uint)available << 9;         // bit 9..11    : available (to the programmer)
    pageTableEntry |= cast(uint)pageAddress;            // bit 12..31   : page frame address (highest 20 bits of a page address)
    
    return pageTableEntry;
}

private ulong createInterruptDescriptorTableEntry(uint interruptServiceRoutinePointer, ushort codeSelector, ubyte privilegeLevel, bool present) {
    assertTrue(privilegeLevel <= 3); // Only 4 (0, 1, 2, 3) privilege levels are possible
    
    ulong interruptDescriptorTableEntry = 0;    
    interruptDescriptorTableEntry |= cast(ulong)(interruptServiceRoutinePointer & 0x0000FFFF) << 0;
    interruptDescriptorTableEntry |= cast(ulong)codeSelector << 16;
    interruptDescriptorTableEntry |= cast(ulong)0xE << 40;
    interruptDescriptorTableEntry |= cast(ulong)privilegeLevel << 45;
    interruptDescriptorTableEntry |= cast(ulong)present << 47;
    interruptDescriptorTableEntry |= cast(ulong)(interruptServiceRoutinePointer >> 16) << 48;
    
    return interruptDescriptorTableEntry;
}

private void cheer() {
    printCharacter('*');
    
    uint *illegalPointer = cast(uint *)0xA00000;
    illegalPointer[0] = 150783;
}

private void interruptServiceRoutine() {
    printCharacter('I');
    printCharacter('S');
    printCharacter('R');
    
    while(true) {
    }
}

private void setupInterrupts() {
    uint *interruptDescriptorTable = cast(uint *)0x105000;
    printCharacter('\n');printCharacter('I');printCharacter('D');printCharacter('T');printCharacter(' ');
    printInteger(cast(uint)interruptDescriptorTable);
    for(int i=0; i<16; i++) {
        ulong entry = createInterruptDescriptorTableEntry(0x00100073, 0x18, 0, true);
        interruptDescriptorTable[i] = entry;
    }    
}
