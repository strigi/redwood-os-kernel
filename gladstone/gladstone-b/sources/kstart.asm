bits 32

global entry

section .text
entry:
enableProtectedMode:
    extern __D4main12printIntegerFkZv
    push dword LINEAR_CODE_SEL
    call __D4main12printIntegerFkZv


	lgdt [gdt_ptr]
	mov eax, cr0
	or eax, 1
	mov cr0, eax
	
enableInterrupts:
    extern __D4main9printLongFmZv
    push dword [idt]
    call __D4main9printLongFmZv

    extern __D4main15setupInterruptsFZv
    call __D4main15setupInterruptsFZv
    lidt [idt_ptr2]
	
initializeDataSection:
	mov ax, LINEAR_DATA_SEL
	mov ds, ax

initializeStackSection:
	mov ax, LINEAR_DATA_SEL
	mov ss, ax
	mov esp, stack
	
initializeCodeSection:
	jmp LINEAR_CODE_SEL:sbat
	
sbat:	
	extern __D4main10kernelMainFPgPgPgPgPgPgZv
	extern stackPageBeginAddress
	extern stackPageEndAddress
	extern dataPageBeginAddress
	extern dataPageEndAddress
	extern codePageBeginAddress
	extern codePageEndAddress
	push dword stackPageEndAddress
	push dword stackPageBeginAddress
	push dword dataPageEndAddress
	push dword dataPageBeginAddress
	push dword codePageEndAddress
	push dword codePageBeginAddress
	call __D4main10kernelMainFPgPgPgPgPgPgZv
	
	; Enables paging
	mov	eax, 0x103000
	mov cr3, eax

    mov eax, cr0
    or eax, 0x80000000
    mov cr0, eax	
	
	extern __D4main5cheerFZv
	call __D4main5cheerFZv
   
	jmp $
	
isr:
    extern __D4main23interruptServiceRoutineFZv
    call __D4main23interruptServiceRoutineFZv

MULTIBOOT_PAGE_ALIGN	equ 1 << 0
MULTIBOOT_MEMORY_INFO	equ 1 << 1
MULTIBOOT_HEADER_MAGIC	equ 0x1BADB002
MULTIBOOT_HEADER_FLAGS	equ MULTIBOOT_PAGE_ALIGN | MULTIBOOT_MEMORY_INFO
MULTIBOOT_CHECKSUM	equ -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)

align 4
multibootHeader:
	dd MULTIBOOT_HEADER_MAGIC
	dd MULTIBOOT_HEADER_FLAGS
	dd MULTIBOOT_CHECKSUM

section .data
gdt:
; NULL descriptor
	dw 0			; limit 15:0
	dw 0			; base 15:0
	db 0			; base 23:16
	db 0			; type
	db 0			; limit 19:16, flags
	db 0			; base 31:24

; unused descriptor
	dw 0
	dw 0
	db 0
	db 0
	db 0
	db 0

LINEAR_DATA_SEL	equ	$-gdt
	dw 0FFFFh
	dw 0
	db 0
	db 92h			; present, ring 0, data, expand-up, writable
	db 0CFh                 ; page-granular (4 gig limit), 32-bit
	db 0

LINEAR_CODE_SEL	equ	$-gdt
	dw 0FFFFh
	dw 0
	db 0
	db 9Ah			; present,ring 0,code,non-conforming,readable
	db 0CFh                 ; page-granular (4 gig limit), 32-bit
	db 0
gdt_end:

gdt_ptr:
	dw gdt_end - gdt - 1
	dd gdt
	
idt:
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E ; 12
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
    
    dw 0x006E
    dw LINEAR_CODE_SEL
    dw 0x8E00
    dw 0x0010
idt_end:

idt_ptr:
    dw idt_end - idt - 1
    dd idt
    
idt_ptr2:
    dw (16 * 8) - 1
    dd 0x105000

section .bss
stack:              ; The stack size is currently determined by the linker script
