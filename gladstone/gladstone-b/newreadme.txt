Binary formats (aka targets):
=============================

Een binary format bestaat uit 2 delen: de file indeling, en de machine architecture (instructieset).
-elf, coff, a.out, bin, pe (aka win32), etc. zijn file indelingen.
- i386, etc. zijn machine architectures die de instructieset bepalen (de opcodes en zo)
==> i368-elf is een mogelijke combinatie. (anderen zijn bv: i686-pe)
(i368 is de x86 architectuur (PC, intel, AMD). Ook i486, i586 en tegenwoordig de meest gebruikte i686 betekent de x86 architectuur, maar deze bevatten mogelijke instructies die sinds nieuwere versies van de x86 processoren beschikbaar zijn (vb sinds de Pentium voor i686).

Er zijn twee plaatsen waar binary formats aan te pas komen:
1. Voor het compileren van source code (*.c, *.asm, ...) naar object files (*.o). Deze object files hebben een bepaald binary format.
-Nasm kan met de -f optie zowat alle binary formats aan (configureerbaar met de command line optie -f (vb -f elf, -f win32, ...).
	-> een lijst van supported targets kan je opvragen door "nasm -hf" te doen
-GCC is specifiek gebuild om een bepaald output format te gebruiken (normaal is dit het binary format van het OS dat op dat moment draait (dus met linux is dit ELF,
met Cygwin op windows is dit PE. Indien je een ander format wilt gebuiken dan dat waar je OS mee werkt spreken we van een CROSS COMPILER (bv een Cygwin die ELF output, of een
Linux die PE (aka win32) output.
	-> een lijst van supported targets kan je opvragen door gcc --target-help te doen. (geeft een lijst van specifieke target switches, met de target format als header bij vermeld (minder overzichtelijk dan nasm).

2. Nadat de object files gegenereerd zijn moet de link stap gebeuren (ld). Deze link stap maakt een executable file (in windows een .exe file). Hier worden alle .o files
gecombineerd (gelinkt) in een enkele file. Deze uiteindelijke file heeft ook een object format (eveneens ELF, PE, COFF, etc). In dit gebal spreekt men ook van "emulation"
De GNU linker (ld) is in staat om de meeste *.o formats te linken (de input files dus). Als output is dit echter analoog aan GCC: de linker is specifiek gebuild om een bepaald
type object format te gebuiken in de uiteindelijke executable files.
	-> een lijst van supported targets kan je opvragen door "ld --help" te doen. De zaken vermeld onder supported targets zijn de mogelijke input binary formats van de *.o files. De zaken vermeld onder supported emulations is de supported output format voor de eigenlijke executable files.