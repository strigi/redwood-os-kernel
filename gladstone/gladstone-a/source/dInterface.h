/**
 * This file contains all the function prototypes that are implemented in D.
 * The idea is to port all of C code to D, so this file will inevitable become more and more obsolete.
 * TODO: when removing a function because it's no longer used from other C files, make sure to put the calling convention in the D file
 * to the regular D convention instead of extern(C).
 */
#ifndef D_INTERFACE_H
#define D_INTERFACE_H

/**
 * See paging.d
 */
extern void initializePaging();

/**
 * See portio.d
 */
extern uint8 writePort(uint16 portAddress, uint8 value);
extern uint8 readPort(uint16 portAddress);


/**
 * See cmos.d
 */
extern uint8 CMOS_REGISTER_REAL_TIME_CLOCK_SECONDS;
extern uint8 CMOS_REGISTER_REAL_TIME_CLOCK_MINUTES;
extern uint8 CMOS_REGISTER_REAL_TIME_CLOCK_HOURS;
extern uint8 CMOS_REGISTER_REAL_TIME_CLOCK_WEEKDAY;
extern uint8 CMOS_REGISTER_REAL_TIME_CLOCK_DAY_OF_MONTH;
extern uint8 CMOS_REGISTER_REAL_TIME_CLOCK_MONTH;
extern uint8 CMOS_REGISTER_REAL_TIME_CLOCK_YEAR;
extern uint8 CMOS_REGISTER_REAL_TIME_CLOCK_CENTURY;
extern uint8 CMOS_REGISTER_REAL_TIME_CLOCK_STATUS_REGISTER_A;
extern uint8 CMOS_REGISTER_REAL_TIME_CLOCK_STATUS_REGISTER_B;

/**
 * See Animal.d
 */
extern void performExperiment();


#endif
