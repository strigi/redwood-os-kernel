module display;

// From display.c
// TODO: port display.c entirely to D
// (possibly in two phases: 1 = extern(C) style, 2 = complete full D)
extern(C) void printString(char* message);
extern(C) void printInteger(int value, byte radix, uint paddingSize);
extern(C) void reportErrorAndHaltSystem(char* message);
extern(C) void printAddress(void *pointer);
extern(C) void printByte(ubyte value);
extern(C) void printBytesAt(void *pointer, int length);
// ===============================================================================================================

void printInteger(int value) {
	printInteger(value, 10);
}

void printInteger(int value, int radix) {
	printInteger(value, radix, 1);
}
