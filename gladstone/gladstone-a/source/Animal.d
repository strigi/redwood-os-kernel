module kevin;

import display;

public class Animal {
	private int length;

	public this(int length) {
		this.length = length;
	}

	public final int doSomething(int factor) {
		printString("doSomething: ");
		int result = factor * length;
		printInteger(result);
		printString("\n");
		return result;
	}		
}

extern(C) void performExperiment() {
	printString("Creating animal\n");

	Animal a = new Animal(1507);
	printString("Address of new animal: ");
	printAddress(cast(void*)a);
	printString("\n");	
	
	int result = a.doSomething(2);
	printString("RESULT: ");
	printInteger(result);
	printString("\n");
}

