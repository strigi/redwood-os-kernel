#include "datetime.h"
#include "dInterface.h"
#include "display.h"

uint8 convertBinaryCodedDecimalToBinary(uint8 binaryCodedDecimalValue) {
	return (binaryCodedDecimalValue & 0x0F) + 10 * ((binaryCodedDecimalValue & 0xF0) >> 4);	
}

DateTime getCurrentDateTime() {
	uint8 statusRegisterB = readCmosRegister(CMOS_REGISTER_REAL_TIME_CLOCK_STATUS_REGISTER_B);
	if(statusRegisterB & 0x2 != 1) {
		reportErrorAndHaltSystem("Unable to read real time clock values from CMOS: Hour is in 12h format. Still need to implement this.");
	}
	
	if(statusRegisterB & 0x4 != 0) {
		reportErrorAndHaltSystem("Unable to read real time clock values from CMOS: Register values are not in Binary-coded decimal form. Still need to implement this.");
	}

	DateTime dateTime;
	
	dateTime.seconds = convertBinaryCodedDecimalToBinary(readCmosRegister(CMOS_REGISTER_REAL_TIME_CLOCK_SECONDS));
	dateTime.minutes = convertBinaryCodedDecimalToBinary(readCmosRegister(CMOS_REGISTER_REAL_TIME_CLOCK_MINUTES));
	dateTime.hours = convertBinaryCodedDecimalToBinary(readCmosRegister(CMOS_REGISTER_REAL_TIME_CLOCK_HOURS));	
	
	uint32 dayOfWeek = convertBinaryCodedDecimalToBinary(readCmosRegister(CMOS_REGISTER_REAL_TIME_CLOCK_WEEKDAY));
	if(dayOfWeek == 1) {
		dayOfWeek = 7;
	} else {
		dayOfWeek--;
	}
	dateTime.dayOfWeek = dayOfWeek;
	
	dateTime.dayOfMonth = convertBinaryCodedDecimalToBinary(readCmosRegister(CMOS_REGISTER_REAL_TIME_CLOCK_DAY_OF_MONTH));
	dateTime.month = convertBinaryCodedDecimalToBinary(readCmosRegister(CMOS_REGISTER_REAL_TIME_CLOCK_MONTH));

	uint32 year = convertBinaryCodedDecimalToBinary(readCmosRegister(CMOS_REGISTER_REAL_TIME_CLOCK_YEAR));
	uint32 century = convertBinaryCodedDecimalToBinary(readCmosRegister(CMOS_REGISTER_REAL_TIME_CLOCK_CENTURY));
	dateTime.year = century * 100 + year;

	return dateTime;
}
