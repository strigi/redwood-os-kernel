import display;

/**
 * Defines the first byte of memory that is not occupied by the kernel itself.
 *
 * This constant should not be used directly. Instead use the END_OF_KERNEL_ADDRESS constant.
 *
 * It is a symbol defined in the linker script. It is marked as C linkage, to make sure
 * D does not mangle the name incorrectly.
 *
 * The value of this byte should be considered garbage. It's address however, is very interesting
 * because it is the first memory address that is free (not occupied by the kernel code, and data sections).
 * The address of the endOfKernel is not page aligned.
 * See_Also: linkerScript.ld
 */
extern(C) extern const byte endOfKernel;

/**
 * Defines the memory address of the first byte that is not occupied by the kernel itself.
 * Note that this value is _not_ page aligned.
 */
const void* END_OF_KERNEL_ADDRESS = cast(void*)&endOfKernel;

/**
 * Defines the maximum available memory for use by the system.
 */
extern(C) const uint MAXIMUM_AVAILABLE_MEMORY = 4 * 1024 * 1024;

/**
 * Defines the size of one page.
 * For x86 processors, this is 4096 bytes.
 */
const int PAGE_SIZE = 1 << 12;

/**
 * Defines how many page table entries can be put in one page table.
 *
 * If you consider a page table as an uint[], this constant defines the length of the array.
 * As each page table in x86 takes 4 bytes, and one page table is 4096 bytes, a page table has
 * a total of 1024 entries.
 */
const int ENTRIES_PER_PAGE_TABLE = PAGE_SIZE / uint.sizeof;

/**
 * Defines the bit in the processor's CR0 register that marks paging.
 *
 * If this bit is set, the CPU will be in paging mode.
 */
const uint CONTROL_REGISTER_PAGING_BIT = 0x80000000;

/**
 * Points to the next free page frame.
 */
void* nextPageFrame = null;

/**
 * Points to the kernel's page directory.
 */
uint* pageDirectory = null;

public extern(C) void initializePaging() {
	printString("Initializing paging...\n");
	nextPageFrame = cast(uint*)roundUpToPageFrame(END_OF_KERNEL_ADDRESS); 
	setupPageDirectory();
	identityMapKernelCodeAndDataSections();
	identityMapPageTables();
	identityMapKernelStack();
	enablePaging();
	
	// Experiment to test if things work
	// If paging works, a PI sign would appear top right corner on screen.
	mapPageFrame(cast(void*)0x000B8000, cast(void*)0x002FF000);
	ushort* videoMemoryTest = cast(ushort*)0x002FF000;
	videoMemoryTest[79] = 1507;
}

/**
 * Initializes the page directory, and fills in all page tables required to map the MAXIMUM_AVAILABLE_MEMORY.
 *
 * The page directory and page tables are allocated directly after the kernel.
 * Setting up the required page tables for the available memory in advance facilitates management of pages later on:
 * we do not have to worry that when a page needs to be mapped, weather or not the required page table for that page
 * is already present in the page directory or not. We can just assume it is.
 */
private void setupPageDirectory() {
	pageDirectory = cast(uint*)allocatePageFrame();

	printString("Page directory: 0x");
	printAddress(pageDirectory);
	printString("\n");

	// TODO: should calculate like this:
	// ceiling(MAXIMUM_AVAILABLE_MEMORY / PAGE_SIZE / ENTRIES_PER_PAGE_TABLE)
	// but the ceiling function does not exist yet due to FPU difficulties???
	int numberOfPageTablesRequiredToMapAllAvailableMemory = MAXIMUM_AVAILABLE_MEMORY / PAGE_SIZE / ENTRIES_PER_PAGE_TABLE;
	
	printString("Need ");
	printInteger(numberOfPageTablesRequiredToMapAllAvailableMemory);
	printString(" page tables to map ");
	printInteger(MAXIMUM_AVAILABLE_MEMORY / 1024 / 1024);
	printString(" MiB of memory\n");

	for(int pageDirectoryIndex = 0; pageDirectoryIndex < numberOfPageTablesRequiredToMapAllAvailableMemory; pageDirectoryIndex++) {
		uint* pageTable = cast(uint*)allocatePageFrame();
		for(int pageTableIndex = 0; pageTableIndex < ENTRIES_PER_PAGE_TABLE; pageTableIndex++) {
			pageTable[pageTableIndex] = createPageTableEntry(null, false, false, true);
		}
		pageDirectory[pageDirectoryIndex] = createPageTableEntry(pageTable, true, true, true);

		printString("Page table: 0x");
		printAddress(pageTable);
		printString("\n");

	}
	for(int pageDirectoryIndex = numberOfPageTablesRequiredToMapAllAvailableMemory; pageDirectoryIndex < ENTRIES_PER_PAGE_TABLE; pageDirectoryIndex++) {
		pageDirectory[pageDirectoryIndex] = createPageTableEntry(null, false, false, true);
	}
}


private void identityMapKernelCodeAndDataSections() {
	void* currentPageFrame = null;
	while(currentPageFrame < roundUpToPageFrame(END_OF_KERNEL_ADDRESS)) {
		mapPageFrame(currentPageFrame, currentPageFrame);
		currentPageFrame += PAGE_SIZE;
	}
}

/**
 * Identity maps the kernel stack to virtual addresses.
 *
 * The kernel stack will be setup at the end of the MAXIMUM_AVAILABLE_MEMORY.
 */
private void identityMapKernelStack() {
	void* bottomOfStack = cast(void*)(MAXIMUM_AVAILABLE_MEMORY - 1);
	void* stackPageFrame = roundDownToPageFrame(bottomOfStack);

	printString("Bottom of stack: ");
	printAddress(bottomOfStack);
	printString("\n");
	printString("Stack page frame: 0x");
	printAddress(stackPageFrame);
	printString("\n");
	
	mapPageFrame(stackPageFrame, stackPageFrame);	
}

private void identityMapPageTables() {
	mapPageFrame(pageDirectory, pageDirectory);

	int pageDirectoryIndex = 0;
	while(isPageFramePresent(pageDirectory, pageDirectoryIndex)) {
		uint* pageTable = getPageTableAddress(pageDirectoryIndex);
/*		
		printAddress(pageTable);
		printString("\n");
*/
		mapPageFrame(pageTable, pageTable);
		pageDirectoryIndex++;
	}
}

/**
 * Extracts the physical memory address of specified page directory entry.
 *
 * Looks up a page table entry in the page directory (thus pointing to a page table), and extracts
 * the reference physical address from it.
 * The result is the address of the page table for the specified index.
 * Params:
 *	pageDirectoryIndex =	The index in the page directory where the desired page table entry can be found.
 * Returns: A pointer to the page table referenced by the specified index.
 */
private uint* getPageTableAddress(int pageDirectoryIndex) {
	return cast(uint*)roundDownToPageFrame(cast(uint*)pageDirectory[pageDirectoryIndex]);
}

/**
 * Checks if a referenced page frame is marked as "present".
 * Params:
 *	pageTable =	The page table to look into.
 *	index =	The index of the page frame entry to find in the specified page table.
 * Returns: True if the specified page frame is marked in the page table as "present". False otherwise.
 */ 
private bool isPageFramePresent(uint* pageTable, int index) {
	return (pageTable[index] & 1) == 1;
}

/**
 * Maps a physical memory address to a virtual memory address.
 *
 * Creates an entry in the page tables, to map a physical memory address to a virtual memory address.
 * Params:
 *	fromPhysicalPageFrame =	The physical page frame address to map to a virtual counterpart.
 *							This value must be page frame aligned.
 *	toVirtualPageFrame =	The virtual page frame address to map the specified physical page frame address to.
 *							This value must be page frame aligned.
 */
private void mapPageFrame(void* fromPhysicalPageFrame, void* toVirtualPageFrame) {
	int pageDirectoryIndex = cast(int)toVirtualPageFrame / (ENTRIES_PER_PAGE_TABLE * PAGE_SIZE);
	int pageTableIndex = (cast(uint)toVirtualPageFrame / PAGE_SIZE) % ENTRIES_PER_PAGE_TABLE;
	uint* pageTable = getPageTableAddress(pageDirectoryIndex);
/*
	printString("map 0x");
	printAddress(fromPhysicalPageFrame);
	printString(" to 0x");
	printAddress(toVirtualPageFrame);
	printString(" PDI=");
	printInteger(pageDirectoryIndex);
	printString(", PTI=");
	printInteger(pageTableIndex);
	printString("\n");
*/
	pageTable[pageTableIndex] = createPageTableEntry(fromPhysicalPageFrame, true, true, true);
}

/**
 * Rounds the specified address downwards to the next page frame boundry.
 *
 * If the address is already page frame aligned, it is returned as is.
 *
 * This is essentially the same as zeroing out the 12 lowest bits.
 * Params:
 *	address =	The address that should be page frame aligned.
 * Returns: The first page frame aligned address lower or equal to the specified parameter.
 */
private void* roundDownToPageFrame(void* address) {
	return cast(void*)(cast(uint)address & ~(PAGE_SIZE - 1));
}

/**
 * Rounds the specified address upwards to the next page frame boundry.
 *
 * If the address is already page frame aligned, it is still rounded upwards to the next page frame boundry.
 * Params:
 *	address =	The address that should be page aligned upwards.
 * Returns: The first page frame aligned address higher then the specified parameter.
 */
private void* roundUpToPageFrame(void* address) {
	return roundDownToPageFrame(address) + PAGE_SIZE;
}

/**
 * Allocates the next available page frame, and marks that as used by the kernel.
 *
 * Any currently free page will be used for this.
 * Do not use this function if you want to control the exact physical address of the page frame you want to get.
 * Returns: The page frame aligned address of the available free page. Contents are not cleared nor initialized.
 */
private void *allocatePageFrame() {
	void* pageFrame = nextPageFrame;
	nextPageFrame += PAGE_SIZE;
	return pageFrame;
}

//TODO:
// For now this is my memory allocator.
// IT ONLY WORKS ON SINGLE PAGE GRANULARITY AND NO DEALLOCATING IS POSSIBLE
// IT IS JUST AN EXPERIMENTAL QUICK AND DIRTY WAY TO GET HEAP MEMORY FOR THE KERNEL!!!!!
// SHOULD BE PUT AND DONE BY AN ACTUAL MEMORY MANAGER, NOT A PAGE ALLOCATOR
public void *allocateMemory() {
	void* pageFrame = allocatePageFrame();
	mapPageFrame(pageFrame, pageFrame);
	return pageFrame;
}

/**
 * Creates an entry in a page table.
 *
 * A page table entry maps a physical page frame to a virtual page frame, together with some
 * properties. It is 4 bytes long.
 * params:
 *	pageFrameAddress =	The physical address of the page frame to map.
 *						This must be a page aligned value (meaning its lowest 12 bits must be zero).
 *	present =			Boolean indicating if this page frame is currently present.
 *						A page frame that is not present will generate a "page fault" when it is accessed
 *						or written to.
 *	readWrite =			Indicates if this page frame can be written to, or if it's read-only.
 *	supervisorOnly =	Specifies if this page frame is a kernel page frame (supervisor) or a user page frame
 *						(not supervisor).
 */
private uint createPageTableEntry(void *pageFrameAddress, bool present, bool readWrite, bool supervisorOnly) {
	uint entry = cast(uint)pageFrameAddress;	
	entry |= cast(uint)present;
	entry |= cast(uint)readWrite << 1;
	entry |= cast(uint)!supervisorOnly << 2;
	return entry;
}

/**
 * Enables paging.
 *
 * Paging is enabled by loading the CR3 register with the
 * page-aligned (4KiB) address of the page directory
 * and then turning on the {@link #CONTROL_REGISTER_PAGING_BIT paging bit} in CR0.
 * See_Also: CONTROL_REGISTER_PAGING_BIT
 */
private void enablePaging() {
	asm {
		mov EAX, pageDirectory;
		mov CR3, EAX;
		
		mov EAX, CR0;
		or EAX, CONTROL_REGISTER_PAGING_BIT;
		mov CR0, EAX;
	}
}






