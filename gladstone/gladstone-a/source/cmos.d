module cmos;

import portio;

public extern(C) const ubyte CMOS_REGISTER_REAL_TIME_CLOCK_SECONDS = 0x00;
public extern(C) const ubyte CMOS_REGISTER_REAL_TIME_CLOCK_MINUTES = 0x02;
public extern(C) const ubyte CMOS_REGISTER_REAL_TIME_CLOCK_HOURS = 0x04;
public extern(C) const ubyte CMOS_REGISTER_REAL_TIME_CLOCK_WEEKDAY = 0x06;
public extern(C) const ubyte CMOS_REGISTER_REAL_TIME_CLOCK_DAY_OF_MONTH = 0x07;
public extern(C) const ubyte CMOS_REGISTER_REAL_TIME_CLOCK_MONTH = 0x08;
public extern(C) const ubyte CMOS_REGISTER_REAL_TIME_CLOCK_YEAR = 0x09;
public extern(C) const ubyte CMOS_REGISTER_REAL_TIME_CLOCK_CENTURY = 0x32;
public extern(C) const ubyte CMOS_REGISTER_REAL_TIME_CLOCK_STATUS_REGISTER_A = 0x0A;
public extern(C) const ubyte CMOS_REGISTER_REAL_TIME_CLOCK_STATUS_REGISTER_B = 0x0B;

/**
 * Defines the control port to the CMOS chip.
 *
 * This is the port address that is used to "select" a CMOS register to read.
 * Selecting a CMOS register is done by writing a byte representing the CMOS register to read
 * to the control port.
 */
private const ushort CMOS_CONTROL_PORT = 0x0070;

/**
 * Defines the data port to the CMOS chip.
 *
 * This is the port address that is used to read a CMOS register.
 * A CMOS register should be selected first (see CMOS_CONTROL_PORT).
 */
private const ushort CMOS_DATA_PORT = 0x0071;

/**
 * Reads data from the specified CMOS register.
 *
 * Reading a CMOS register is done by first selecting a register by writing a byte
 * (which represents the CMOS register to read) to port {@link CMOS_CONTROL_PORT},
 * and then reading the port {@link CMOS_DATA_PORT} to retrieve the register's value.
 *
 * Params:
 * 	cmosRegister =	The CMOS register to read.
 * Returns: The value of the specified CMOS register.
 */
public extern(C) ubyte readCmosRegister(ubyte cmosRegister) {
	writePort(CMOS_CONTROL_PORT, cmosRegister);
	return readPort(CMOS_DATA_PORT);
}

