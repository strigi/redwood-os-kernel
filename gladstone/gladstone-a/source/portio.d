module portio;

/**
 * Reads a byte from the port with the specified address.
 *
 * Reading from an IO port is done like this:
 * 	- First load the address to read in the DX (16bit) register.
 *	- Then call the 'in <register>, DX' asm instruction, to read the value in the specified register.
 *	- Return the value of the specified register.
 *
 * Implementation note: For readability reasons, the read value is first stored in the output variable "result", and then returned.
 * Although using the "EAX for return values" (e.g. 'C') calling convention would allow this step to be omitted if we call 'in AX, DX',
 * since then the EAX value would be populated with the read value already. However, since this assumes the reader of this code to have
 * good knowledge of calling conventions, and also assumes this particular calling convention to be always used, we work with a temporary
 * result variable instead, which is probably safer, and also much more readable.
 *
 * Params:
 *	portAddress =	The 16bit address of the port to read from.
 * Returns: The value read from the port.
 */
public extern(C) ubyte readPort(ushort portAddress) {
	ubyte result;
	asm {
		mov DX, portAddress;
		in AL, DX;
		mov result, AL;
	}
	return result;
}

/**
 * Writes a byte to the port with the specified address.
 *
 * Writing to an IO port is done like this:
 *	- First load the address to write to in the DX (16bit) register.
 *	- Then load an 8bit register with the value to write.
 *	- Finally call the 'out DX, <register>' asm instruction.
 *
 * Params:
 *	portAddress =	The 16bit address of the port to write to.
 *	value =			The value to write to the specified port.
 */
public extern(C) void writePort(ushort portAddress, ubyte value) {
	asm {
		mov DX, portAddress;
		mov AL, value;
		out DX, AL;
	}
}

