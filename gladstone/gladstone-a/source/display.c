#include "display.h"

uint16 *videoMemory = (uint16 *)0x000B8000;

int currentPosition = 0;

FontColor currentColor = {
	.foregroundColor = WHITE,
	.backgroundColor = BLACK
};

void reportErrorAndHaltSystem(char errorMessage[]) {
	FontColor color = {.foregroundColor = LIGHT_RED, .backgroundColor = DARK_RED};
	setColor(color);
	printString(errorMessage);
	haltSystem();
}

void reportInfo(char infoMessage[]) {
	FontColor previousColor = getColor();
	FontColor infoColor = {.foregroundColor = LIGHT_GREEN, .backgroundColor = DARK_GREEN};
	setColor(infoColor);
	printString(infoMessage);
	setColor(previousColor);
}

FontColor getColor() {
	return currentColor;
}


void setColor(FontColor fontColor) {
	currentColor = fontColor;
}

int absoluteValue(int value) {
	return value >= 0 ? value : -value;
}

uint32 power(uint32 base, uint32 exponent) {
	if(exponent == 0) {
		return 1;
	}
	
	uint32 result = base;
	for(uint32 i = 1; i < exponent; i++) {
		result *= base;
	}
	return result;
}

void setPosition(int position) {
	currentPosition = position;
}

int getPosition() {
	return currentPosition;
}

unsigned short buildColor(FontColor fontColor) {
	return (fontColor.backgroundColor << 4) + fontColor.foregroundColor;
}

uint16 buildCharacter(char character, FontColor fontColor) {
	return (buildColor(fontColor) << 8) + character;
}

void copyMemory(void *fromAddress, void *toAddress, uint32 size) {
	int toIndex = 0;
	for(uint32 fromIndex = 0; fromIndex < size; fromIndex++) {
		((uint8 *)toAddress)[toIndex++] = ((uint8 *)fromAddress)[fromIndex];
	}
}

void scroll() {
	copyMemory(videoMemory + DISPLAY_COLUMNS, videoMemory, sizeof(uint16) * DISPLAY_COLUMNS * (DISPLAY_LINES - 1));
	
	for(int i = 0; i < DISPLAY_COLUMNS; i++) {
		videoMemory[(DISPLAY_LINES - 1) * DISPLAY_COLUMNS + i] = buildCharacter(' ', currentColor);
	}
	
	setPosition((DISPLAY_LINES - 1) * DISPLAY_COLUMNS);	
}

uint8 isAtEndOfScreenBuffer() {
	return currentPosition >= DISPLAY_COLUMNS * DISPLAY_LINES;
}

void printChar(char value) {
	if(isAtEndOfScreenBuffer()) {
		scroll();
	}

	if(value == '\n') {
		int currentLine = (currentPosition / DISPLAY_COLUMNS);
		setPosition((currentLine + 1) * DISPLAY_COLUMNS);
	} else {
		videoMemory[currentPosition++] = buildCharacter(value, currentColor);
	}
}

void printString(char message[]) {
	for(int i = 0; message[i] != '\0'; i++) {
		printChar(message[i]);
	}
}

void printInt(int value, int radix) {
	// Check correctness of input parameters.
	if(radix != 2 && radix != 8 && radix != 10 && radix != 16) {
		reportErrorAndHaltSystem("Invalid radix specified");
	}

	char availableDigits[] = "0123456789ABCDEF";
	
	// The largest possible integer (2^32 - 1), printed in the most wide format (binary)
	// will require 32 characters, so allocate a buffer of that size to temporary store output characters.
	char buffer[32];

	int bufferIndex = 0;
	int workValue = value;	
	
	// If the value is zero, just print '0'.
	if(workValue == 0) {
		printChar('0');
		return;
	}	
	
	// Build the string in reverse order first,
	// taking the last digit each time.
	workValue = absoluteValue(workValue);	
	while(workValue != 0) {
		char digit = availableDigits[workValue % radix];
		buffer[bufferIndex++] = digit;
		workValue /= radix;
	}	
	// If it's a negative number, add the '-' character.
	if(value < 0) {
		buffer[bufferIndex++] = '-';
	}
	
	// The buffer now contains the inverted string, so we need to print out this buffer in reverse order.
	for(int i = bufferIndex-1; i >= 0; i--) {
		printChar(buffer[i]);
	}
}

void clearScreen() {
	setPosition(0);
	for(uint32 i = 0; i < DISPLAY_COLUMNS * DISPLAY_LINES; i++) {
		printChar(' ');
	}
	setPosition(0);
}

void printDateTime(DateTime dateTime) {
	int dateTimeRadix = 10;
	printInteger(dateTime.year, dateTimeRadix, 4);
	printChar('-');
	printInteger(dateTime.month, dateTimeRadix, 2);
	printChar('-');
	printInteger(dateTime.dayOfMonth, dateTimeRadix, 2);
	printChar(' ');
	printInteger(dateTime.hours, dateTimeRadix, 2);
	printChar(':');
	printInteger(dateTime.minutes, dateTimeRadix, 2);
	printChar(':');
	printInteger(dateTime.seconds, dateTimeRadix, 2);
}

void printAddress(void *pointer) {
	printInteger((int)pointer, 16, 8);
}

void printByte(uint8 byte) {
	printInteger(byte, 16, 2);
}

void printBytesAt(void *pointer, int length) {
	for(int i = 0; i < length; i++) {
		if(i != 0) {
			printChar(' ');
		}
		uint8 value = ((uint8 *)pointer)[i];
		printInteger(value, 16, 2);
	}
}

void printInteger(int value, uint8 radix, uint32 paddingSize) {
	if(paddingSize > 0) {
		int smallestNumberOfPaddingSize = power(radix, paddingSize - 1);
		while(smallestNumberOfPaddingSize > value && smallestNumberOfPaddingSize > 1) {
			printChar('0');
			smallestNumberOfPaddingSize /= radix;
		}
	}
	printInt(value, radix);
	
}

