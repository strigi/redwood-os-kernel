#ifndef DISPLAY_H
#define DISPLAY_H

#include "datetime.h"

#define DISPLAY_COLUMNS 80
#define DISPLAY_LINES 25

typedef enum {
	BLACK = 0x0,
	DARK_BLUE = 0x1,
	DARK_GREEN = 0x2,
	DARK_TEAL = 0x3,
	DARK_RED = 0x4,
	MAGENTA = 0x5,
	ORANGE = 0x6,
	LIGHT_GRAY = 0x7,
	DARK_GRAY = 0x8,
	LIGHT_BLUE = 0x9,
	LIGHT_GREEN = 0xA,
	LIGHT_TEAL = 0xB,
	LIGHT_RED = 0xC,
	LIGHT_MAGENTA = 0xD,
	YELLOW = 0xE,
	WHITE = 0xF
} Color;

typedef struct {
	Color foregroundColor;
	Color backgroundColor;
} FontColor;


// TODO: find a home for this
int absoluteValue(int value);
/**
 * Calculates the exponent of two values.
 * The formula is as follows: "base ^ exponent" (base to the power of exponent)
 * Example: power(2, 4) would yield 16.
 * @param base The base of the exponentiation.
 * @param the exponent or power of the exponentiation.
 * @return The base to the power of exponent.
 */
uint32 power(uint32 base, uint32 exponent);


// TODO: find a home for this
extern void haltSystem();
void reportErrorAndHaltSystem(char errorMessage[]);
void reportInfo(char infoMessage[]);



void setColor(FontColor fontColor);
FontColor getColor();

void setPosition(int position);
int getPosition();
unsigned short buildColor(FontColor fontColor);
unsigned short buildCharacter(char character, FontColor fontColor);
void printChar(char value);
void printString(char message[]);
void printInt(int value, int radix);
void clearScreen();
void printDateTime(DateTime dateTime);
void printAddress(void *pointer);
void printByte(unsigned char byte);
void printBytesAt(void *pointer, int length);

/**
 * Prints an integer value to the screen.
 * The value can be formatted in multiple representations: binary, octal, decimal or hexadecimal.
 * Left padding of '0' characters can be specified as well.
 * @param value The integer value to print.
 * @param radix The radix for the format to use: 2 for binary, 8 for octal, 10 for decimal and 16 for hexadecimal.
 * @param paddingSize The minimum number of characters to 'reserve' for printing this integer. If the number to print requires less characters than the paddingSize,
 * leading zeros will be printed. If the number is larger than the paddingSize, The complete value is displayed (no trimming is done).
 */
void printInteger(int value, uint8 radix, uint32 paddingSize);


#endif
