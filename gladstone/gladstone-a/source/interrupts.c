#include "interrupts.h"
#include "display.h"
#include "dInterface.h"

#define MASTER_PROGRAMMABLE_INTERRUPT_CONTROLLER_COMMAND_PORT 0x0020
#define MASTER_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT 0x0021
#define SLAVE_PROGRAMMABLE_INTERRUPT_CONTROLLER_COMMAND_PORT 0x00A0
#define SLAVE_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT 0x00A1


#define SYSTEM_TIMER_MASK 1
#define KEYBOARD_MASK 2
#define IRQ2_MASK 4
#define IRQ3_MASK 8
#define IRQ4_MASK 16
#define IRQ5_MASK 32
#define IRQ6_MASK 64
#define IRQ7_MASK 128
#define IRQ8_MASK 256
#define IRQ9_MASK 512
#define IRQ10_MASK 1024
#define IRQ11_MASK 2048
#define IRQ12_MASK 4096
#define IRQ13_MASK 8192
#define IRQ14_MASK 16384
#define IRQ15_MASK 32768


/**
 * Defines the offset in the interrupt descriptor table where the 16 hardware interrupts are to be mapped.
 * All 16 hardware interrupts are mapped in sequential order, so the first hardware interrupt (IRQ0) will map to interrupt {@link FIRST_HARDWARE_INTERRUPT_INDEX}
 * and the last will map to interrupt ({@link FIRST_HARDWARE_INTERRUPT_INDEX} + (16 - 1)).
 */
#define FIRST_HARDWARE_INTERRUPT_INDEX 0x20

/**
 * The interrupt descriptor table indices
 */
#define DIVISION_BY_ZERO_INTERRUPT_INDEX 0x00
#define PAGE_FAULT_INTERRUPT_INDEX 0x0E
#define SYSTEM_TIMER_INTERRUPT_INDEX (FIRST_HARDWARE_INTERRUPT_INDEX + 0)
#define KEYBOARD_INTERRUPT_INDEX (FIRST_HARDWARE_INTERRUPT_INDEX + 1)

uint64 createInterruptDescriptorTableEntry(uint32 present, uint32 descriptorPrivilegeLevel, uint32 codeSelector, void *interruptServiceRoutineCallback) {
	uint64 entry = 0;
	entry |= ((uint64)(int)interruptServiceRoutineCallback) & 0x000000000000FFFF;
	entry |= (((uint64)(int)interruptServiceRoutineCallback) & 0x00000000FFFF0000) << (4 * 8);
	entry |= (((uint64)codeSelector) & 0x000000000000FFFF) << (2 * 8);
	entry |= (((uint64)present) & 0x0000000000000001) << (5 * 8) + 7;
	entry |= (((uint64)descriptorPrivilegeLevel) & 0x0000000000000003) << (5 * 8) + 5;
	entry |= ((uint64)0xE) << (5 * 8);
	return entry;
}

uint64 createInterruptDescriptorTablePointer(uint64 interruptDescriptorTable[], int length) {
	uint64 pointer = 0;
	pointer |= (uint64)(length * 8);
	pointer |= ((uint64)(int)interruptDescriptorTable) << (2 * 8);
	return pointer;
}

void setHardwareInterruptMask(uint16 mask) {
	writePort(MASTER_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT, mask);
	writePort(SLAVE_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT, mask >> 8);
}

uint16 getHardwareInterruptMask() {
	return (readPort(SLAVE_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT) << 8) +
			readPort(MASTER_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT);
}

void remapHardwareInterrupts(uint8 hardwareInterruptIndexInInterruptDescriptorTable) {
	// EXPERIMENTAL: remap the PIC so that IRQs do not overlap with protected mode exceptions.
	// TODO: does it still work otherwise? What happens if you don't, besides not knowing which is the trigger, does it still work?
	// Setup init mode for the PIC
	writePort(MASTER_PROGRAMMABLE_INTERRUPT_CONTROLLER_COMMAND_PORT, 0x11);
	writePort(SLAVE_PROGRAMMABLE_INTERRUPT_CONTROLLER_COMMAND_PORT, 0x11);
	
	// Remap offsets for the pics
	writePort(MASTER_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT, hardwareInterruptIndexInInterruptDescriptorTable);
	writePort(SLAVE_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT, hardwareInterruptIndexInInterruptDescriptorTable + 8);
	
	// Setup master / slave interrupt gates??
	writePort(MASTER_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT, 0x04);
	writePort(SLAVE_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT, 0x02);
	
	// Enable 8086 mode
	writePort(MASTER_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT, 0x01);
	writePort(SLAVE_PROGRAMMABLE_INTERRUPT_CONTROLLER_DATA_PORT, 0x01);
}

void installInterruptDescriptorTable() {
	// Setup the interrupt descriptor table with interrupt handlers
	static uint64 interruptDescriptorTable[INTERRUPT_DESCRIPTOR_TABLE_LENGTH];
	for(int i = 0; i < INTERRUPT_DESCRIPTOR_TABLE_LENGTH; i++) {
		interruptDescriptorTable[i] = createInterruptDescriptorTableEntry(1, 0, 0x10, (void *)&dummyInterruptCallbackWrapper);
	}
	interruptDescriptorTable[DIVISION_BY_ZERO_INTERRUPT_INDEX] = createInterruptDescriptorTableEntry(1, 0, 0x10, (void *)&divisionByZeroInterruptCallbackWrapper);
	interruptDescriptorTable[PAGE_FAULT_INTERRUPT_INDEX] = createInterruptDescriptorTableEntry(1, 0, 0x10, (void *)&pageFaultInterruptCallbackWrapper);
	interruptDescriptorTable[SYSTEM_TIMER_INTERRUPT_INDEX] = createInterruptDescriptorTableEntry(1, 0, 0x10, (void *)&systemTimerInterruptCallbackWrapper);
	interruptDescriptorTable[KEYBOARD_INTERRUPT_INDEX] = createInterruptDescriptorTableEntry(1, 0, 0x10, (void *)&keyboardInterruptCallbackWrapper);
	
	// Load the newly created interrupt descriptor table
	static uint64 interruptDescriptorTablePointer = 0;
	interruptDescriptorTablePointer = createInterruptDescriptorTablePointer(interruptDescriptorTable, INTERRUPT_DESCRIPTOR_TABLE_LENGTH);
	loadInterruptDescriptorTable(interruptDescriptorTablePointer);
}

void initializeInterrupts() {
	printString("Initializing interrupts...\n");
	disableHardwareInterrupts();	
	installInterruptDescriptorTable();
	remapHardwareInterrupts(FIRST_HARDWARE_INTERRUPT_INDEX);
	setHardwareInterruptMask(0xFFFF & ~KEYBOARD_MASK);
	enableHardwareInterrupts();
}

/**
 * Handles a division by zero exception.
 * This is a normal function that is called by (and returned to) the matching interrupt wrapper.
 */
void divisionByZeroInterruptServiceRoutineCallback() {
	reportErrorAndHaltSystem("Division by zero");
}

void pageFaultInterruptServiceRoutineCallback() {
	reportErrorAndHaltSystem("PAGE FAULT");
}

void dummyInterruptServiceRoutineCallback() {
	reportInfo("DUMMY INTERRUPT INVOKED");
}

void systemTimerInterruptServiceRoutineCallback() {
	reportInfo("SYSTEM TIMER");
}

/**
 * Handles a keyboard interrupt.
 * This is a normal function that is called by (and returned to) the matching interrupt wrapper.
 */
void keyboardInterruptServiceRoutineCallback() {
//	reportInfo("KEYBOARD PRESSED");
	
	// Read scancode
	uint8 scanCode = readPort(0x60);
	
	static uint32 commandFlag = 0;

	switch(scanCode) {
		case 0x10:
			printChar('a');
			break;
		case 0x11:
			printChar('z');
			break;
		case 0x12:
			printChar('e');
			break;
		case 0x13:
			printChar('r');
			break;
		case 0x14:
			if(commandFlag) {
				printDateTime(getCurrentDateTime());
			} else {
				printChar('t');
			}			
			break;
		case 0x15:
			printChar('y');
			break;
		case 0x16:
			printChar('u');
			break;
		case 0x17:
			printChar('i');
			break;
		case 0x18:
			printChar('o');
			break;
		case 0x19:
			printChar('p');
			break;
		case 0x1C:
			printChar('\n');
			break;
		case 0x1D: // LEFT CONTROL
			commandFlag = 1;
			break;
		case 0x1E:
			printChar('q');
			break;
		case 0x1F:
			printChar('s');
			break;
		case 0x20:
			printChar('d');
			break;
		case 0x21:
			printChar('f');
			break;
		case 0x22:
			printChar('g');
			break;
		case 0x23:
			printChar('h');
			break;
		case 0x24:
			printChar('j');
			break;
		case 0x25:
			printChar('k');
			break;
		case 0x26:
			printChar('l');
			break;
		case 0x27:
			printChar('m');
			break;
		case 0x2C:
			printChar('w');
			break;			
		case 0x2D:
			printChar('x');
			break;
		case 0x2E:
			printChar('c');
			break;
		case 0x2F:
			printChar('v');
			break;
		case 0x30:
			printChar('b');
			break;
		case 0x31:
			printChar('n');
			break;
		case 0x39:
			printChar(' ');
			break;
		case 0x9D: // LEFT CONTROL UP
			commandFlag = 0;
			break;
	}
		
	// Send IRQ acknowledge (aka EOI, End Of Interrupt)
	// TODO: This should be done for all (except for fake) hardware interrupts
	int endOfInterrupt = 0x20;
	writePort(MASTER_PROGRAMMABLE_INTERRUPT_CONTROLLER_COMMAND_PORT, endOfInterrupt);
	
}

