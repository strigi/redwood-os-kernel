#ifndef INTERRUPTS_H
#define INTERRUPTS_H

#include "types.h"

/**
 * Defines the length of the interrupt descriptor vector table.
 * The length of this table equals the amount of interrupts that are mapped in the interrupt descriptor table.
 * Interrupts use a zero based index.
 */
#define INTERRUPT_DESCRIPTOR_TABLE_LENGTH 256

/**
 * Loads a new interrupt descriptor table.
 * This is done by calling the "lidt" instruction to load the address of the
 * "interrupt descriptor table pointer" structure into the "idtr" register.
 * @param 1st [esp + 4] 8 bytes: The interrupt descriptor table pointer (not it's address!) to use.
 * Actually this is only a 6 byte structure, but it's padded with two zero bytes in the end,
 * to be more conventient to work with.
 */
extern void loadInterruptDescriptorTable(uint64 interruptDescriptorTablePointer);

// Special interrupt wrappers
// These are not normal function calls, they are interrupt handler, which
// are not called manually! They are called with an "int" instruction and return with "iret"
// instead of "call" and "ret". So do not call these manually!!!!!
// I want to get rid of this. (see todo in asm file)
extern void divisionByZeroInterruptCallbackWrapper();
extern void keyboardInterruptCallbackWrapper();
extern void dummyInterruptCallbackWrapper();	
extern void systemTimerInterruptCallbackWrapper();
extern void pageFaultInterruptCallbackWrapper();

/**
 * Creates the interrupt descriptor table pointer (IDTP).
 * This is a 48 bits datastructure (for convenience we use an uint64, but we leave the two most
 * significant bytes (the ones with the highest address) to zero).
 * The data structure looks like this:
 *  - 0..15 (word) = the size of the IDT in bytes (always a multiple of 8, as in #entries * 8).
 *  - 16..47 (dword) = the address of IDT.
 */
uint64 createInterruptDescriptorTablePointer(uint64 interruptDescriptorTable[], int length);

/**
 * Creates a single entry for the interrupt descriptor table.
 * An interrupt descriptor table entry determines where the interrupt service routine for this interrupt resides,
 * and with which properties it is to be invoked.
 * The index of the interrupt that is described by this entry is determined not in this structure itself, but in
 * the position (index) of this structure inside the interrupt descriptor table (IDT).
 * @param present A single bit (0 or 1) to indicate if this vector is available.
 * @param descriptorPrivilegeLevel Determines the privilege level (aka "ring") of this interrupt routine. This can be either 0, 1, 2 or 3,
 * with 0 being the most privileged level.
 * @param codeSelector The code selector is the index of the code segment in the global descriptor table. Currently it seems to be ok to pass in 0x10.
 * That's probably because GRUB sets it's own GDT, where the code segment is the third entry in the GDT. (I figured this out by reading the value of the CS register).
 * However, that still needs to be fixed: I need to setup my own GDT.
 * @param interruptServiceRoutineCallback This is the pointer to the interrupt service routine. Note that this (currently) can not just point to an actual
 * c function, because an ISR is invoked with the "int" instruction, and needs to return with the "iret" instruction. Which do different things (namely with the stack pointer)
 * than a regular "call" and "ret". For this reason, I currently need this pointer to be an assembler-written "wrapper" that calls the real function inside of it.
 * I really want to do away with that, because it implies a separate wrapper for each actual ISR.
 * @return A 64bit interrupt descriptor table entry. This can be used to populate the interrupt descriptor table.
 */
uint64 createInterruptDescriptorTableEntry(uint32 present, uint32 descriptorPrivilegeLevel, uint32 codeSelector, void *interruptServiceRoutineCallback);

void initializeInterrupts();

extern void enableHardwareInterrupts();
extern void disableHardwareInterrupts();

#endif
