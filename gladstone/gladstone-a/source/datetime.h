#ifndef DATE_TIME_H
#define DATE_TIME_H

#include "types.h"

typedef struct {
	/**
	 * Defines the seconds of the current minute. Range from 0 to 59.
	 */
	uint8 seconds;
	
	/**
	 * Defines the minutes of the current hour. Range from 0 to 59.
	 */	 
	uint8 minutes;
	
	/**
	 * Defines the hours of the current day in 24 hour format. Range from 0 to 23.
	 */
	uint8 hours;
	
	/**
	 * Defines the day of the current month. Range from 1 to 31.
	 */
	uint8 dayOfMonth;
	
	/**
	 * Defines the day of week. Range from 1 (for monday) to 7 (for sunday).
	 * Note that this field is said to be "unreliable". However, let's assume this is no longer an issue for modern hardware,
	 * so we'll use it anyway. If it turns out to be a problem afterall, we'll fix that then.
	 */
	uint8 dayOfWeek;
	
	/**
	 * Defines the month of the current year. Range from 1 to 12.
	 */
	uint8 month;
	
	/**
	 * Defines the current year in 4 digit form (e.g. '2012').
	 */
	uint16 year;
} DateTime;

DateTime getCurrentDateTime();

#endif
