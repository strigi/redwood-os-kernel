[bits 32]

; The very start of the kernel.
; Code execution will start here.
[extern kernelMain]
[extern MAXIMUM_AVAILABLE_MEMORY]
start:
	; The very first thing to do is setup the stack, so we can safely call functions.
	;
	; The stack is set to the very end of the memory available to the system.
	; This is currently controlled by the constant MAXIMUM_AVAILABLE_MEMORY in "paging.d".
	;
	; Example: If the maximum memory is 4MiB, then the stack should be on the very last
	; address (0x003FFFFF) of the last page frame (0x003FF000). So it will be the same
	; as MAXIMUM_AVAILABLE_MEMORY - 1.
	mov eax, [MAXIMUM_AVAILABLE_MEMORY]
	dec eax
	mov esp, eax
	
	; TODO: setup a custom GDT, not the one from GRUB (which you can't know where it's at, and
	; could be overridden accidently)
	; Also, this would alow me to use a code segment for the IDT entries that is not guessed at (0x10)
	; (i took that value from taking a look at the CS register, but i'm not sure if this is reliable)
	call kernelMain
	call disableHardwareInterrupts
	call haltSystem

; Halts the system.
; Halting the system means disabling all interrupts (cli instruction) and putting the CPU into HALT mode (hlt instruction).
; This will cause the CPU not to consume (much?) power.
[global haltSystem]
haltSystem:
	hlt
	
; Defines the GRUB multiboot header.
; The multiboot header is used by GRUB to identify and invoke the kernel image.
; This header must be within the first 8KB of the kernel image, and must be 4 byte aligned.
; In it's most minimal form (the one we're using) the multiboot header only consists of three 4 byte values:
;	- A magic number (this is always 0x1BADB002)
;	- Flags used to specify if GRUB should pass video and memory info to the kernel, and - in case we use a binary format for our kernel image
;		that is unknown to GRUB - where the .text, .data and .bss sections start.
;		We don't need any of these flags, because we don't need memory and video info to be passed to the kernel (yet?),
;		and we're using the ELF format as the kernel image, which is "known" to GRUB already. So, our flags dword is 0x00000000.
;	- A checksum, which must be the negative value of the sum of the magic number and the flags.
align 4
multibootHeader:
	dd 0x1BADB002	; Magic number
	dd 0x00000000	; Flags
	dd -0x1BADB002	; Checksum

; @see interrupts.h
[global loadInterruptDescriptorTable]
loadInterruptDescriptorTable:
	lidt [esp + 4]
	ret

; EXPERIMENTAL CODE
; same deal as with divisionByZeroInterruptServiceRoutineCallback
[extern divisionByZeroInterruptServiceRoutineCallback]
[global divisionByZeroInterruptCallbackWrapper]
divisionByZeroInterruptCallbackWrapper:
	; TODO: need to find a way to pass the address of the callback function based on the interrupt vector
	; or something else that can be passed in some way. That would allow me to setup an array of
	; function pointers in C and use that to map the interrupt vectors to.
	; currently i need a special "interrupt routine wrapper" call like this one here, to bootstrap
	; a call to the actual c interrupt routine handler, which sucks :(
	call divisionByZeroInterruptServiceRoutineCallback
	iret

[extern keyboardInterruptServiceRoutineCallback]
[global keyboardInterruptCallbackWrapper]
keyboardInterruptCallbackWrapper:
	; TODO: they say that hardware interrupts and exceptions should never modify any register
	; so we should push and pop before calling our c callback.
	; i'm prepared to ignore that, to see what happens. If it becomes a problem, i'll fix it then.
	call keyboardInterruptServiceRoutineCallback
	iret
	
[extern systemTimerInterruptServiceRoutineCallback]
[global systemTimerInterruptCallbackWrapper]
systemTimerInterruptCallbackWrapper:
	call systemTimerInterruptServiceRoutineCallback
	iret
	
[extern dummyInterruptServiceRoutineCallback]
[global dummyInterruptCallbackWrapper]
dummyInterruptCallbackWrapper:
	call dummyInterruptServiceRoutineCallback
	iret
	
[extern pageFaultInterruptServiceRoutineCallback]
[global pageFaultInterruptCallbackWrapper]
pageFaultInterruptCallbackWrapper:
	call pageFaultInterruptServiceRoutineCallback
	iret
	
[global enableHardwareInterrupts]
enableHardwareInterrupts:
	sti
	ret
	
[global disableHardwareInterrupts]
disableHardwareInterrupts:
	cli
	ret
	
[global retrieveProcessorStatusFlags]
retrieveProcessorStatusFlags:
	pushfd
	mov eax, [esp]
	popfd
	ret



