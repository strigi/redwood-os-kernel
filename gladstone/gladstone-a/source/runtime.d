import paging;
import display;

// druntime: extern (C) Object _d_newclass(ClassInfo ci)
extern(C) Object _d_newclass() {
	void* pageFrame = allocateMemory();
	printString("Allocate new object @ ");
	printAddress(pageFrame);
	printString("\n");
	return cast(Object)pageFrame;
}

// druntime: extern (C) void _d_newclass(ClassInfo ci)
extern(C) void _D9invariant12_d_invariantFC6ObjectZv(Object o) {
	printString("CALLED '_D9invariant12_d_invariantFC6ObjectZv'\n");
}



// UNKNOWN STUFF: THESE MAY NOT EVEN BE FUNCTION CALLS
extern(C) void __gdc_personality_v0() {
	reportErrorAndHaltSystem("CALLED '__gdc_personality_v0'\n");
}

extern(C) void _d_array_bounds() {
	reportErrorAndHaltSystem("CALLED '_d_array_bounds'\n");
}

extern(C) void _d_assert_msg(char[] message) {
	reportErrorAndHaltSystem("CALLED '_d_assert_msg'\n");
}

extern(C) void _d_assert(bool condition) {
	reportErrorAndHaltSystem("CALLED '_d_assert'\n");
}

extern(C) void _D6Object7__ClassZ() {
	reportErrorAndHaltSystem("CALLED '_D6Object7__ClassZ'\n");
}

extern(C) void _D6object6Object5printMFZv() {
	reportErrorAndHaltSystem("CALLED '_D6object6Object5printMFZv'\n");
}

extern(C) void _D6object6Object8toStringMFZAa() {
	reportErrorAndHaltSystem("CALLED '_D6object6Object8toStringMFZAa'\n");
}

extern(C) void _D6object6Object6toHashMFZk() {
	reportErrorAndHaltSystem("CALLED '_D6object6Object6toHashMFZk'\n");
}

extern(C) void _D6object6Object5opCmpMFC6ObjectZi() {
	reportErrorAndHaltSystem("CALLED '_D6object6Object5opCmpMFC6ObjectZi'\n");
}

extern(C) void _D6object6Object8opEqualsMFC6ObjectZi() {
	reportErrorAndHaltSystem("CALLED '_D6object6Object8opEqualsMFC6ObjectZi'\n");
}

extern(C) void _D9ClassInfo6__vtblZ() {
	// NOT A FUNCTION
	reportErrorAndHaltSystem("CALLED '_D9ClassInfo6__vtblZ'\n");
}

extern(C) void _D14TypeInfo_Class6__vtblZ() {
	// NOT A FUNCTION
	reportErrorAndHaltSystem("CALLED '_D14TypeInfo_Class6__vtblZ'\n");
}
