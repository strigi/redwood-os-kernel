#include "display.h"
#include "datetime.h"
#include "interrupts.h"
#include "types.h"
#include "dInterface.h"

#define CPU_STATUS_FLAG_INTERRUPTS_ENABLED 0x200

// Defined in bootstrap.asm
extern uint32 retrieveProcessorStatusFlags();
extern void haltSystem();

void checkInterruptsEnabled() {
	if(retrieveProcessorStatusFlags() & CPU_STATUS_FLAG_INTERRUPTS_ENABLED) {
		reportInfo("Interrupts are ENABLED\n");
	} else {
		reportInfo("Interrupts are DISABLED\n");
	}
}

void printWelcomeMessage() {
	FontColor color = {.foregroundColor = WHITE, .backgroundColor = DARK_BLUE};
	setColor(color);
	clearScreen();
	printString("Welcome to Gladstone Kernel!\n");
	printString("============================\n");
	color.foregroundColor = LIGHT_GRAY;
	setColor(color);
	printString("A new and better OS\nTime: ");
	printDateTime(getCurrentDateTime());
	printString("\n\n");
}

void kernelMain() {
	printWelcomeMessage();
	
	initializePaging();
	initializeInterrupts();	
	
	reportInfo("PERFORMING EXPERIMENT\n");
	performExperiment();
	
	reportInfo("REACHED END OF KERNEL");
		
	while(1) {
		haltSystem();
	}
}
