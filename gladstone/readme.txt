Gladstone is an other of my OS experiments. This time for x86. Gladstone A and B are two branches of the same root (i can not remember what the difference is or why - that should be sorted out by merging the two).

As redwood is actually my Raspberry Pi OS experiment they are not compatible. It would be cool if I could somehow merge these two together at some point as well though.

For now, I just put all my OS experiments under the redwood repository, so I don't loose any previous work. Who knows - someone may find it and have some use for it at some point).